index = Faker.String.base64() |> Base.encode64() |> String.downcase()

Application.put_env(:eidetic_projector_elasticsearch, :index, index)

config = %{
  uri: "http://elasticsearch:9200",
  index: "my_index"
}

Application.put_env(:bungee, :config, config)
Bungee.start_link(:bungee)
ExUnit.start()
