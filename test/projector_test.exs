defmodule Eidetic.Projector.ElasticsearchTest do
  use ExUnit.Case, async: false

  alias Eidetic.Event, as: EideticEvent
  alias Eidetic.Projector.ConsumedEvent
  alias Eidetic.Projector.ConsumedEvent.Repository, as: ConsumedEventRepository

  ##
  # Test 1 - Are we properly proxying the handler methods?
  # Test 2 - Given there is nothing in consumed events, there should be after handle_message (When configuration is idempotence: :true)
  # Test 3 - Given there is nothing in consumed events, there should not be after handle_message (When configuration is idempotence: :false)
  # Test 4 - Given there is a consumed event in consumed events, there shouldn not be two after handle_message (When configuration is idempotence: :true)
  # Test 5 - Given there is a consumed event in consumed events,
  #           there shouldn not be two after handle_message (When configuration is idempotence: :false) and the event should be replayed also
  ##
  setup do
    eidetic_event = %EideticEvent{
      identifier: UUID.uuid4(),
      serial_number: :rand.uniform(10000),
      payload: %{}
    }

    kafka_event = %{
      value: Poison.encode!(eidetic_event)
    }

    {:ok, %{eidetic_event: eidetic_event, kafka_event: kafka_event}}
  end

  def handle_event(event = %EideticEvent{}, _) do
    File.write("/tmp/#{event.identifier}_#{event.serial_number}", "", [:binary])
  end

  test ~s/that we proxy events to their configured handler/, %{
    eidetic_event: eidetic_event,
    kafka_event: kafka_event
  } do
    Application.put_env(
      :eidetic_projector_elasticsearch,
      :handler,
      Eidetic.Projector.ElasticsearchTest
    )

    {:ok, pid} = Eidetic.Projector.Elasticsearch.start_link()

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)

    assert File.exists?("/tmp/#{eidetic_event.identifier}_#{eidetic_event.serial_number}")

    Application.delete_env(:eidetic_projector_elasticsearch, :handler)

    Process.exit(pid, :kill)
  end

  test ~s/that events are marked as consumed when idempotence is true/, %{
    eidetic_event: eidetic_event,
    kafka_event: kafka_event
  } do
    Application.put_env(:eidetic_projector_elasticsearch, :idempotence, true)
    {:ok, pid} = Eidetic.Projector.Elasticsearch.start_link()

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)

    :timer.sleep(2000)

    assert {:ok, :fetch, _} =
             ConsumedEventRepository.fetch(
               "#{eidetic_event.identifier}:#{eidetic_event.serial_number}"
             )

    Process.exit(pid, :kill)
  end

  test ~s/that events are not marked as consumed when idempotence is false/, %{
    eidetic_event: eidetic_event,
    kafka_event: kafka_event
  } do
    Application.put_env(:eidetic_projector_elasticsearch, :idempotence, false)
    {:ok, pid} = Eidetic.Projector.Elasticsearch.start_link()

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)

    :timer.sleep(2000)

    assert {:ok, :not_found} =
             ConsumedEventRepository.fetch(
               "#{eidetic_event.identifier}:#{eidetic_event.serial_number}"
             )

    Process.exit(pid, :kill)
  end

  test ~s/that a consumed event is replayed with idempotence but marked as a rerun/, %{
    eidetic_event: eidetic_event,
    kafka_event: kafka_event
  } do
    Application.put_env(:eidetic_projector_elasticsearch, :idempotence, true)

    {:ok, pid} = Eidetic.Projector.Elasticsearch.start_link()
    :erlang.trace(pid, true, [:receive])

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)
    assert_receive {:trace, _, :receive, {_, _, {:handle_message, eidetic_event, false}}}

    :timer.sleep(2000)

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)
    assert_receive {:trace, _, :receive, {_, _, {:handle_message, eidetic_event, true}}}
  end

  test ~s/that an event is replayed with idempotence off/, %{
    eidetic_event: eidetic_event,
    kafka_event: kafka_event
  } do
    Application.put_env(:eidetic_projector_elasticsearch, :idempotence, false)

    {:ok, pid} = Eidetic.Projector.Elasticsearch.start_link()
    :erlang.trace(pid, true, [:receive])

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)
    assert_receive {:trace, _, :receive, {_, _, {:handle_message, eidetic_event, false}}}

    assert :ok == Eidetic.Projector.Elasticsearch.handle_message(kafka_event)
    assert_receive {:trace, _, :receive, {_, _, {:handle_message, eidetic_event, false}}}
  end
end
