defmodule Eidetic.Projector.Elasticsearch.Mixfile do
  use Mix.Project

  def project do
    [
      app: :eidetic_projector_elasticsearch,
      version: "0.0.1",
      elixir: "~> 1.5",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),
      description: description(),
      package: package(),
      test_coverage: [tool: ExCoveralls],
      docs: [main: "readme", extras: ["README.md"]]
    ]
  end

  def application do
    [
      applications: [
        :hackney,
        :logger,
        :tesla
      ]
    ]
  end

  def deps do
    [
      {:bungee, "~> 1.0.0-alpha1"},
      {:eidetic, "~> 1.0.0-alpha1", override: true},
      {:kaffe, "~> 1.5"},
      {:poison, "~> 3.1"},
      {:uuid, "~> 1.1"},
      {:credo, "~> 0.8", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:excoveralls, "~> 0.7", only: :test},
      {:faker, "~> 0.9", only: :test}
    ]
  end

  def aliases do
    [test: ["coveralls"]]
  end

  defp description do
    """
    Message Consumer for Eidetic (ElasticSearch)
    """
  end

  defp package do
    [
      name: :eidetic_consumer_elasticsearch,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["GT8Online"],
      licenses: ["MIT"],
      links: %{
        "Source Code" =>
          "https://gitlab.com/gt8/open-source/elixir/eidetic-consumer-elasticsearch"
      }
    ]
  end

  defp elixirc_paths(:dev), do: ["lib"]
  defp elixirc_paths(:test), do: ["test", "examples"] ++ elixirc_paths(:dev)
  defp elixirc_paths(_), do: ["lib"]
end
