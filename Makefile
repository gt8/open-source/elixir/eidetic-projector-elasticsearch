TESTS=

.PHONY: test deps

init:
	@mix local.hex --force
	@mix local.rebar --force

lint:
	@mix format --check-formatted

format:
	@mix format --check-equivalent

deps:
	@mix deps.get

compile:
	@mix compile

test:
	@mix test $(TESTS)

analyse:
	@mix dialyzer

clean:
	@mix clean --deps

# Work within Docker
dshell:
	@docker-compose run --rm --entrypoint=bash elixir

dclean:
	@docker-compose run --rm elixir clean --deps
	@docker-compose down -v
