defmodule Eidetic.Projector.ConsumedEvent do
  alias Eidetic.Event, as: EideticEvent

  defstruct identifier: nil, serial_number: nil, datetime: nil

  def key(consumed_event = %__MODULE__{}) do
    consumed_event.identifier <> ":" <> Integer.to_string(consumed_event.serial_number)
  end

  def create(event = %EideticEvent{}) do
    %__MODULE__{
      identifier: event.identifier,
      serial_number: event.serial_number,
      datetime: DateTime.utc_now()
    }
  end
end
