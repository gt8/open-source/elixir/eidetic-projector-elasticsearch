defmodule Eidetic.Projector.Elasticsearch do
  use GenServer

  alias Eidetic.Event, as: EideticEvent
  alias Eidetic.Projector.ConsumedEvent
  alias Eidetic.Projector.ConsumedEvent.Repository, as: ConsumedEventRepository

  @spec start_link() :: {:ok, pid()}
  @spec init(map()) :: {:ok, map()}
  @spec handle_message(%EideticEvent{}) :: :ok
  @spec handle_call({:has_been_processed?, %EideticEvent{}}, pid(), map()) ::
          {:reply, :handled, map()} | {:reply, %EideticEvent{}, map()}
  @spec has_been_processed?(%EideticEvent{}) :: :handled | %EideticEvent{}
  @spec mark_as_processed(%EideticEvent{} | :handled | :ok) :: :ok
  @spec handle_cast({:mark_as_processed, %EideticEvent{}}, map()) :: {:noreply, map()}
  @spec handle_message!(%EideticEvent{} | :handled) :: %EideticEvent{} | :ok
  @spec handle_call({:handle_message, %EideticEvent{}}, pid(), map()) ::
          {:reply, %EideticEvent{}, map()}

  def start_link do
    idempotence = Application.get_env(:eidetic_projector_elasticsearch, :idempotence, false)
    handler = Application.get_env(:eidetic_projector_elasticsearch, :handler, nil)

    GenServer.start_link(
      __MODULE__,
      %{idempotence: idempotence, handler: handler},
      name: :eidetic_projector_elasticsearch
    )
  end

  def init(state) do
    {:ok, state}
  end

  def handle_message(message = %{}) do
    message[:value]
    |> Poison.decode!(as: %EideticEvent{})
    |> has_been_processed?()
    |> handle_message!()
    |> mark_as_processed()
  end

  defp handle_message!(%{handled: handled, event: event}) do
    GenServer.call(:eidetic_projector_elasticsearch, {:handle_message, event, handled})
  end

  # If no handler is provided, just return the event
  def handle_call(
        {:handle_message, event = %EideticEvent{}, handled},
        _from,
        state = %{handler: nil}
      ) do
    {:reply, %{event: event, handled: handled}, state}
  end

  # Otherwise, call the `handle_event` method on the handler
  def handle_call(
        {:handle_message, event = %EideticEvent{}, handled},
        _from,
        state = %{handler: handler}
      ) do
    handler.handle_event(event, rerun: handled)
    {:reply, %{event: event, handled: handled}, state}
  end

  # When `idempotence` is disabled, we just return the event; which means this
  # event "hasn't" been handled yet.
  def handle_call(
        {:has_been_processed?, event = %EideticEvent{}},
        _from,
        state = %{idempotence: false}
      ) do
    {:reply, %{event: event, handled: false}, state}
  end

  # When `idempotence` is enabled, we'll do a quick look-up in Elasticsearch
  # We may wish to speed this up by also maintaining a cache in our state
  def handle_call(
        {:has_been_processed?, event = %EideticEvent{}},
        _from,
        state = %{idempotence: true}
      ) do
    {:ok, :found, _} = ConsumedEventRepository.fetch("#{event.identifier}:#{event.serial_number}")

    {:reply, %{handled: true, event: event}, state}
  rescue
    _ -> {:reply, %{event: event, handled: false}, state}
  end

  defp has_been_processed?(
         event = %EideticEvent{identifier: identifier, serial_number: serial_number}
       ) do
    GenServer.call(:eidetic_projector_elasticsearch, {:has_been_processed?, event})
  end

  defp mark_as_processed(:ok) do
    :ok
  end

  defp mark_as_processed(%{event: event, handled: handled}) do
    GenServer.cast(:eidetic_projector_elasticsearch, {:mark_as_processed, event, handled})

    :ok
  end

  def handle_cast(
        {:mark_as_processed, event = %EideticEvent{}, handled = false},
        state = %{idempotence: true}
      ) do
    {:ok, _, saved_event} =
      event
      |> ConsumedEvent.create()
      |> ConsumedEventRepository.save()

    {:noreply, state}
  end

  def handle_cast(
        {:mark_as_processed, event = %EideticEvent{}, handled = true},
        state = %{idempotence: true}
      ) do
    {:noreply, state}
  end

  def handle_cast({:mark_as_processed, event = %EideticEvent{}, _}, state = %{idempotence: false}) do
    {:noreply, state}
  end
end
