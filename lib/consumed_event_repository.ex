defmodule Eidetic.Projector.ConsumedEvent.Repository do
  use Bungee.Repository,
    module: Eidetic.Projector.ConsumedEvent,
    # A `nil` value for `:index` will result in using whatever `:index` Bungee is configured with
    index: Application.get_env(:eidetic_projector_elasticsearch, :index, nil),
    type: "consumed-events"
end
